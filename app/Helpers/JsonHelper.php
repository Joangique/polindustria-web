<?php


namespace App\Helpers;


class JsonHelper
{

    public static function json_success($message,$data=[],$statuscode=200){
        if ($data == []){
            $statuscode = 204;
        }
        return response()->json([
            "status"=>"success",
            "message"=>$message,
            "data"=>$data
        ],$statuscode,[],256);
    }
    public static function json_warning($message,$data=[],$statuscode=206){
        if ($data == []){
            $statuscode = 203;
        }
        return response()->json([
            "status"=>"warning",
            "message"=>$message,
            "data"=>$data
        ],$statuscode,[],256);
    }
    public static function json_error($message,$statuscode=500){
        return response()->json([
           "status"=>"error",
           "message"=>$message,
            "data"=>null
        ],$statuscode,[],256);
    }
}
