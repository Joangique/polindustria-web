<?php

namespace App\Http\Controllers;

use App\Helpers\JsonHelper;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function list(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');
    }

    public function register(Request $request){
        $email = $request->input('email');
        $name = $request->input('name');
        $password = $request->input('password');
    }

    public function login(Request $request){

        if ($request->input('email') == null || $request->input('password') == null){
            return JsonHelper::json_error("Complete los campos correctamente.");
        }

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email',$email)->where('password',$password)->first();

        if ($user == null){
            return JsonHelper::json_warning("Usuario o contraseña errada");
        }
        $user->remember_token=md5(rand());
        $user->save();
        return JsonHelper::json_success("Usuario autenticado",$user);
    }
}
