<?php

namespace App\Http\Controllers;

use App\Helpers\JsonHelper;
use App\Horario;
use App\HorarioDia;
use App\Personal;
use Faker\Provider\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    public function list()
    {
        return JsonHelper::json_success("Listado de horarios", Horario::all());
    }

    public function horarioPersonal($id)
    {
        $personal = Personal::where('id',$id)->first();
        if ($personal == null){
            return JsonHelper::json_warning("Este personal no se encuentra en nuestros registros");
        }

        $data = HorarioDia::select('horario_dias.*')
            ->join('horarios', 'horarios.id', '=', 'horario_dias.horario_id')
            ->join('personals', 'personals.horario_id', '=', 'horarios.id')
            ->where('personals.id', $id)->get();

        if (count($data) == 0){
            return JsonHelper::json_warning("No existe horario registrado para $personal->apellidos $personal->nombres");
        }
        return JsonHelper::json_success("Horario correspondiente a $personal->apellidos $personal->nombres", $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Horario $horario
     * @return \Illuminate\Http\Response
     */
    public function show(Horario $horario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Horario $horario
     * @return \Illuminate\Http\Response
     */
    public function edit(Horario $horario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Horario $horario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Horario $horario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Horario $horario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Horario $horario)
    {
        //
    }
}
