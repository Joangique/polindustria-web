<?php

namespace App\Http\Controllers;

use App\OrdenesTrabajo;
use Illuminate\Http\Request;

class OrdenesTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrdenesTrabajo  $ordenesTrabajo
     * @return \Illuminate\Http\Response
     */
    public function show(OrdenesTrabajo $ordenesTrabajo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrdenesTrabajo  $ordenesTrabajo
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdenesTrabajo $ordenesTrabajo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrdenesTrabajo  $ordenesTrabajo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdenesTrabajo $ordenesTrabajo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrdenesTrabajo  $ordenesTrabajo
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrdenesTrabajo $ordenesTrabajo)
    {
        //
    }
}
