<?php

namespace App\Http\Controllers;

use App\HorarioDia;
use Illuminate\Http\Request;

class HorarioDiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HorarioDia  $horarioDia
     * @return \Illuminate\Http\Response
     */
    public function show(HorarioDia $horarioDia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HorarioDia  $horarioDia
     * @return \Illuminate\Http\Response
     */
    public function edit(HorarioDia $horarioDia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HorarioDia  $horarioDia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HorarioDia $horarioDia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HorarioDia  $horarioDia
     * @return \Illuminate\Http\Response
     */
    public function destroy(HorarioDia $horarioDia)
    {
        //
    }
}
