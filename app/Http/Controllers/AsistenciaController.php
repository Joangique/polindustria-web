<?php

namespace App\Http\Controllers;

use App\Asistencia;
use App\HorarioDia;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Utils\StringUtil;

class AsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $dias = array("domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado");
        $dia_str = $dias[date("w")];

        $user_id = $request->input('user_id');
        $fecha = Carbon::now()->subHour(5);

        $user = User::find($user_id)->first();
        $horario_id = $user->horario_id;

        $horario_dias = HorarioDia::where('horario_id', $horario_id)->where('dia', $dia_str)->get();

        $acum = [];

        

        foreach ($horario_dias as $item) {
            return Carbon::create(
                $fecha->year, $fecha->month, $fecha->day, explode(':', $item->hora_entrada)[0], explode(':', $item->hora_entrada)[1]
            )->diffInMinutes($fecha);
        }

//        return $horario_dias;

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     */
    public function show(Asistencia $asistencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Asistencia $asistencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asistencia $asistencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asistencia $asistencia)
    {
        //
    }
}
