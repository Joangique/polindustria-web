<?php

namespace Utils;

class StringUtil{

    public static function left($str, $length) {
        return substr($str, 0, $length);
    }

    public static function right($str, $length) {
        return substr($str, -$length);
    }

}
