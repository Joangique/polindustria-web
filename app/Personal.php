<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{

    protected $fillable = [
        'nombres',
        'apellidos',
        'numdoc',
        'tipdoc_id',
        'cargo_id',
        'horario_id',
        'estado',
    ];

    public function tipdoc(){
        return $this->belongsTo(Tipodocumento::class,'tipdoc_id');
    }
    public function cargo(){
        return $this->belongsTo(Cargo::class,'cargo_id');
    }
    public function horario(){
        return $this->belongsTo(Horario::class,'cargo_id');
    }
    public function estado(){
        return $this->belongsTo(EstadoPersonal::class,'estado_id')->where('muestra',true);
    }
}
