@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
           <div class="col-12">
               <div class="card">
                   <div class="card-header">
                       Listado de personal
                   </div>
                   <div class="card-body">
                       <div class="table-responsive-vertical">
                       <table class="">
                           <thead>
                           <tr>
                               <th>Id</th>
                               <th>Nombres</th>
                               <th>Apellidos</th>
                               <th>Numero Doc</th>
                               <th>Cargo</th>
                               <th>Horario</th>
                               <th>Estado</th>
                               <th>Acciones</th>
                           </tr>
                           </thead>
                           <tbody>
                           @foreach($list as $item)
                               <tr>
                                   <td data-title="id">{{$item->id}}</td>
                                   <td data-title="nombres">{{$item->nombres}}</td>
                                   <td data-title="apellidos">{{$item->apellidos}}</td>
                                   <td data-title="nombre">{!! "<b>".$item->tipdoc->nombre."</b> ".$item->numdoc !!}</td>
                                   <td data-title="cargo">{{$item->cargo->titulo}}</td>
                                   <td data-title="horario">{{$item->horario->titulo}}</td>
                                   <td data-title="estado">{{$item->estado}}</td>
                                   <td>
                                       <button class="btn btn-success btn-sm">Editar</button>
                                       <button class="btn btn-danger btn-sm">Eliminar</button>
                                   </td>
                               </tr>
                           @endforeach
                           </tbody>
                       </table>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </div>
@endsection
