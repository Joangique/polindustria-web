<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="w-100 h-100">
    <div class="wrapper">

        <div class="sidebar">
            <ul class="menu">
                <li class="menu__item"><a class="menu__link" href="/personal">Personal</a></li>
                <li class="menu__item"><a class="menu__link" href="">Item</a></li>
                <li class="menu__item"><a class="menu__link" href="">Item</a></li>
                <li class="menu__item"><a class="menu__link" href="">Item</a></li>
            </ul>
        </div>

        <div class="main">
            @include('shared/navbar')
            <div class="container p-2">
                @yield('content')
            </div>
        </div>
    </div>
    {{--        <div class="wrapper ">--}}
    {{--            <div class="sidebar">--}}
    {{--                <ul class="menu">--}}
    {{--                    <li class="menu__item"><a class="menu__link" href="/personal">Personal</a></li>--}}
    {{--                    <li class="menu__item"><a class="menu__link" href="">Item</a></li>--}}
    {{--                    <li class="menu__item"><a class="menu__link" href="">Item</a></li>--}}
    {{--                    <li class="menu__item"><a class="menu__link" href="">Item</a></li>--}}
    {{--                </ul>--}}
    {{--            </div>--}}
    {{--            <main class="main">--}}
    {{--                @yield('content')--}}

    {{--            </main>--}}
    {{--        </div>--}}
</div>
</body>
</html>
