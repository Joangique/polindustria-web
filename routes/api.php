<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v2'],function (){
    Route::post('login','UserController@login');
    Route::resource('cargos','CargoController');
    Route::resource('horarios','HorarioController');

    Route::get('personal','PersonalController@list');
    Route::get('horario/personal/{id}','HorarioController@horarioPersonal');
    Route::get('horario/dias/{id}','HorarioController@horariodias');
});
