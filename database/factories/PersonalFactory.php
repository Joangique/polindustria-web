<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Personal;
use Faker\Generator as Faker;

$factory->define(Personal::class, function (Faker $faker) {
    return [
        'nombres'=>$faker->firstName,
        'apellidos'=>$faker->lastName,
        'numdoc'=>$faker->randomNumber(8),
        'tipdoc_id'=>1,
        'cargo_id'=>1,
        'horario_id'=>1,
        'estado_id'=>rand(1,2),
        'image'=>'http://picsum.photos/id/'.rand(1,999).'/640/480.jpg',
    ];
});
