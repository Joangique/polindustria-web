<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('personal_id')->nullable();
            $table->foreign('personal_id')->on('personals')->references('id');
            $table->unsignedBigInteger('horario_dia_id')->nullable();
            $table->foreign('horario_dia_id')->on('horario_dias')->references('id');
            $table->unsignedBigInteger('ordenes_trabajo_id')->nullable();
            $table->foreign('ordenes_trabajo_id')->on('ordenes_trabajos')->references('id');
            $table->date('fecha');
            $table->time('hora');
            $table->integer('diferencia_minutos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
}
