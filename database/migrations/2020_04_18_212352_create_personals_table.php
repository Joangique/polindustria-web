<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('numdoc');
            $table->unsignedBigInteger('tipdoc_id');
            $table->foreign('tipdoc_id')->on('tipodocumentos')->references('id');
            $table->unsignedBigInteger('cargo_id')->nullable();
            $table->foreign('cargo_id')->on('cargos')->references('id');
            $table->unsignedBigInteger('horario_id')->nullable();
            $table->foreign('horario_id')->on('horarios')->references('id');

            $table->unsignedBigInteger('estado_id');
            $table->foreign('estado_id')->on('estado_personals')->references('id');


            $table->text('image')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
    }
}
