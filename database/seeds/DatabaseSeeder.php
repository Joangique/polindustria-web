<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Jolver Andre Gil Quevedo',
            'email'=>'devneonix@gmail.com',
            'password'=>'Manchita'
        ]);
        \App\Cargo::create(['titulo'=>'Administrador']);
        \App\Cargo::create(['titulo'=>'Produccion']);
        \App\Cargo::create(['titulo'=>'Obrero']);

        \App\Horario::create(['titulo'=>'Horario OBRERO']);
        \App\Horario::create(['titulo'=>'Horario OFICINA']);


        \App\Tipodocumento::create(['nombre'=>'DNI']);
        \App\EstadoPersonal::create(['titulo'=>'Activo']);
        \App\EstadoPersonal::create(['titulo'=>'Inactivo','muestra'=>0]);

        $dias = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];
        foreach ($dias as $dia){
            \App\HorarioDia::create([
                'horario_id'=>1,
                'dia'=>$dia,
                'hora_entrada'=>'09:00',
                'hora_salida'=>'19:21'
            ]);
        }



        factory(App\Personal::class,500)->create();

    }
}
